package com.example.dress;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.dress.model.Product;
import com.example.dress.model.User;
import com.example.dress.model.productRepository;
import com.example.dress.model.userRepository;
@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(path="api/v1/prod")
public class productController {
	private final productRepository repository ;
	private  final userRepository repository1  ;

	

	  productController(productRepository repository,userRepository repository1) {
		    this.repository = repository;
			this.repository1 = repository1;
		  }
	@CrossOrigin("*")
	@RequestMapping(value="/prod",method= {RequestMethod.GET, RequestMethod.POST})
	public   org.springframework.http.ResponseEntity.BodyBuilder uplaodImage(@RequestParam(value="id",required = false,defaultValue="0") String id,@RequestParam(value="name",required = false,defaultValue="0") String name,@RequestParam(value="image",required = false,defaultValue="0") MultipartFile file,@RequestParam(value="price",required = false,defaultValue="0") String price,@RequestParam(value="description",required = false,defaultValue="0") String description) throws IOException {
		Long id1 =Long.parseLong(id);
		System.out.println("l id du product " + id1);
		User user=new User();
		Optional<User> user1=	this.repository1.findById(id1);
		user=user1.get();

			System.out.println("product controller user  " + user.getFirst_name());


		//Optional<Product> prod1 = this.repository.findById(id1);
		Product prod = new Product();
		//prod=prod1.get();
		
		
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		prod.setUser(user);
		prod.setPictureType(file.getContentType());
		prod.setImage(compressBytes(file.getBytes()));
		prod.setDescription(description);
		prod.setName(name);
		prod.setPrice(price);
		

		this.repository.save(prod);
		return  ResponseEntity.status(HttpStatus.OK);
	}
	@CrossOrigin("*")
	@RequestMapping(value ="/prod1",method= {RequestMethod.GET, RequestMethod.POST})
	public  Product[] getUser(@RequestParam(value="id",required=false,defaultValue = "0") String id) throws IOException {
		System.out.println("id fo product get method "+id);
		ArrayList<Product> retrievedImage=new ArrayList<>();
		Product[] prod1s =new Product[100];
		Long id1=Long.parseLong(id);
		 retrievedImage = (ArrayList<Product>) this.repository.findAll();
		 for(int i=0;i<retrievedImage.size();i++) {
			 System.out.println( retrievedImage.get(i).getName());
			Product prod =new Product(decompressBytes(retrievedImage.get(i).getImage()));
			prod1s[i]=prod;
		 }
	/*	Product prod = new Product(retrievedImage.get().getId(),retrievedImage.get().getName(),decompressBytes(retrievedImage.get().getImage()),retrievedImage.get().getPictureType(),
		retrievedImage.get().getPrice(),retrievedImage.get().getDescription(),retrievedImage.get().getUser());
		System.out.println("GetController Method - " + decompressBytes(retrievedImage.get().getImage()));*/

		return prod1s;
	}

	// compress the image bytes before storing it in the database
	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

		return outputStream.toByteArray();
	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
	private final static String PATH="/error";
	private Product [] prod1s;
	 String getErrorPath() {
		return PATH;
		 
	 }
	 @RequestMapping(value=PATH,method=RequestMethod.GET)
	 public String defaultErrorMessag() {
		return "Requested Resource is not found!";
		 
	 }
	

}
