package com.example.dress;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.dress.model.User;
import com.example.dress.model.userRepository;
@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(path="api/v1")
public class imageUploadController {
	@Autowired
	 private final userRepository repository;
	  imageUploadController(userRepository repository) {
		    this.repository = repository;
		  }
	@CrossOrigin("*")
	@RequestMapping(value="/upload",method= {RequestMethod.GET, RequestMethod.POST})
	public   org.springframework.http.ResponseEntity.BodyBuilder uplaodImage(@RequestParam(value="id",required = false,defaultValue="0") String id,@RequestParam(value="logo",required = false,defaultValue="0") MultipartFile file) throws IOException {
		
		Long id1 =Long.parseLong(id);
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		User user ;
		Optional<User> user1 = this.repository.findById(id1);
		
		user=user1.get();
		
		
		System.out.println(user.getFirst_name());
		
		user.setPicture(file.getName());
		user.setPictureType(file.getContentType());
		user.setLogo(compressBytes(file.getBytes()));

		this.repository.save(user);
		return  ResponseEntity.status(HttpStatus.OK);
	}
	@CrossOrigin("*")
	@RequestMapping(value ="/upload1",method= {RequestMethod.GET, RequestMethod.POST})
	public  User getUser(@RequestParam(value="id",required=false,defaultValue = "0") String id) throws IOException {
		System.out.println(id);

		Long id1=Long.parseLong(id);
	 Optional<User> retrievedImage = this.repository.findById(id1);
		User user = new User(retrievedImage.get().getId(),retrievedImage.get().getLast_name(),retrievedImage.get().getFirst_name(),retrievedImage.get().getAddress(),retrievedImage.get().getUsername(),retrievedImage.get().getPassword(),retrievedImage.get().getPicture(), retrievedImage.get().getPictureType(),
				decompressBytes(retrievedImage.get().getLogo()), retrievedImage.get().getProduct());
		System.out.println("GetController Method - " + decompressBytes(retrievedImage.get().getLogo()));

		return user;
	}

	// compress the image bytes before storing it in the database
	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

		return outputStream.toByteArray();
	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
	private final static String PATH="/error";
	 String getErrorPath() {
		return PATH;
		 
	 }
	 @RequestMapping(value=PATH,method=RequestMethod.GET)
	 public String defaultErrorMessag() {
		return "Requested Resource is not found!";
		 
	 }
	

}
