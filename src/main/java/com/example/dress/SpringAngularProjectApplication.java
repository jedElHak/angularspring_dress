package com.example.dress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan(basePackages ={"com.example.controller"})
@EnableAutoConfiguration(exclude = ErrorMvcAutoConfiguration.class)
@ComponentScan(basePackages ={"com.example..dress.config"})
@SpringBootApplication
public class SpringAngularProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAngularProjectApplication.class, args);
	}

}
