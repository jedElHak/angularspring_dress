package com.example.dress.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="product")
@Getter
@Setter
@ToString
public class Product implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id ;
	private String name ;
	private byte[] image;
	private String pictureType;
	public Product(Long id, String name, byte[] image, String pictureType, String price, String description,
			User user) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.pictureType = pictureType;
		this.price = price;
		this.description = description;
		this.user = user;
	}
	private String price ;
	private String description ;
	@ManyToOne()
	@JoinColumn(name="user_id",nullable=false)
    @JsonBackReference
	private User user ;
	public Product(byte[] image) {
		super();
		this.image = image;
	}
	public Product(Long id, String name, String picture, String price, String description, User user) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.user = user;
	}
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getPictureType() {
		return pictureType;
	}
	public void setPictureType(String pictureType) {
		this.pictureType = pictureType;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}


}
