package com.example.dress.model;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="user")
@Getter
@Setter
@ToString
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id ;
	private String last_name;
	private String first_name;
	private String address;
	@Column(name="username")
	private String username;
	@Column(name="password")
	private String password;
	private String picture;
	private String pictureType;
	public String getPictureType() {
		return pictureType;
	}



	public void setPictureType(String pictureType) {
		this.pictureType = pictureType;
	}


	@Column(name = "logo", length = 2000)
	private byte []logo; 
    @JsonBackReference
	@OneToMany(cascade = CascadeType.ALL,mappedBy="user")
	 private  Set<Product> product ;
	public User() {
		super();
	}
	
	

	public byte[] getLogo() {
		return logo;
	}



	public void setLogo(byte[] logo) {
		this.logo = logo;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLast_name() {
		return last_name;
	}



	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}



	public String getFirst_name() {
		return first_name;
	}



	public Set<Product> getProduct() {
		return product;
	}



	public void setProduct(Set<Product> product) {
		this.product = product;
	}



	public User(Long id, String last_name, String first_name, String address, String username, String password,
			String picture, String pictureType, byte[] logo, Set<Product> product) {
		super();
		this.id = id;
		this.last_name = last_name;
		this.first_name = first_name;
		this.address = address;
		this.username = username;
		this.password = password;
		this.picture = picture;
		this.pictureType = pictureType;
		this.logo = logo;
		this.product = product;
	}



	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}



	public String getPicture() {
		return picture;
	}



	public void setPicture(String picture) {
		this.picture = picture;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public User(Long id, String picture, String pictureType, byte[] logo) {
		super();
		this.id = id;
		this.picture = picture;
		this.pictureType = pictureType;
		this.logo = logo;
	}




	
	

}
