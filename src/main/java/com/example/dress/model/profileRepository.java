package com.example.dress.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
@CrossOrigin("http://localhost:4200")
public interface profileRepository extends JpaRepository<Profile, Long> {
  Page<Profile> findById(@Param("id")Long id,Pageable page);
}
